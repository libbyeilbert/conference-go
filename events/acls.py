from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_photo(city, state):
    try:
        url = "https://api.pexels.com/v1/search"
        params = {"query": city + " " + state, "per_page": 1}
        headers = {"Authorization": PEXELS_API_KEY}

        res = requests.get(url, params=params, headers=headers)
        unencoded = json.loads(res.content)
        url = unencoded["photos"][0]["url"]
        return {"picture_url": url}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather_data(city, state):
    url = (
        "http://api.openweathermap.org/geo/1.0/direct?q="
        + city
        + ","
        + state
        + ",unitedstates&limit=1&appid="
        + OPEN_WEATHER_API_KEY
    )
    res = requests.get(url)
    weather = json.loads(res.content)
    latitude = str(weather[0]["lat"])
    longitude = str(weather[0]["lon"])
    url = (
        "https://api.openweathermap.org/data/2.5/weather?lat="
        + latitude
        + "&lon="
        + longitude
        + "&appid="
        + OPEN_WEATHER_API_KEY
        + "&units=imperial"
    )
    r = requests.get(url)
    weather = json.loads(r.content)
    weather_dict = {
        "temperature": weather["main"]["temp"],
        "description": weather["weather"][0]["description"],
    }
    return weather_dict
